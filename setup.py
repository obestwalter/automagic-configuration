from setuptools import setup, find_packages

setup(
    name="example-projects",
    packages=find_packages(),
    entry_points={
        "console_scripts": [
            "a-very-simple-config=a_module_level_constants.app:some_important_function",
            "a-simple-config=b_namespace_class.app:some_important_function",
            "b-properties-config=c_singleton_instance_with_properties.app:some_important_function",
            "c-descriptors-config=d_descriptors_annotations_config.app:main",
            "d-rocket-science-config=e_freezable_descriptor_based_singleton.app:some_important_function",
        ],
    },
)
