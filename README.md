# Automagic Configuration

This is the example Code from the talk "Automagic Configuration". Last version was done for the [WeAreDevelopers Python Day](https://www.wearedevelopers.com/event/python-day-0902). The talk is mainly about the basic ideas and mechanics leading up to this on a more introductory level. The code here is there for you to explore and see what might be the right level of magic in the context of your project.

The video of the talk is [here](https://www.wearedevelopers.com/en/videos/automagic-configuration-in-python).

# How to use this example code

You can use tox to run the tests and demos. tox is a commandline tool, which can be installed e.g. via [pipx](https://pypi.org/project/pipx/):

    $ pipx install tox
    $ git@gitlab.com:obestwalter/automagic-configuration.git
    $ cd automagic-configuration
    $ tox -av

    using tox.ini: .../automagic-configuration/tox.ini (pid 346606)
    using tox-3.23.0 from .../tox/__init__.py (pid 346606)
    default environments:
    black -> reformat all code in /home/ob/do/automagic-configuration with black
    a     -> test a_module_level_constants
    b     -> test b_namespace_class
    c     -> test c_singleton_instance_with_properties
    d     -> test d_descriptors_annotations_config
    e     -> test e_freezable_descriptor_based_singleton

run all tests and demos

    $ tox

```text
All done! ✨ 🍰 ✨
37 files left unchanged.
============================= test session starts ==============================
platform linux -- Python 3.8.10, pytest-7.0.1, pluggy-1.0.0 -- /home/ob/do/automagic-configuration/.venv/bin/python
cachedir: .venv/.pytest_cache
rootdir: /home/ob/do/automagic-configuration, configfile: tox.ini
plugins: xonsh-0.11.0
collecting ... collected 18 items

tests/a_module_level_constants/test_config_import_directly.py::test_output_different_systems[Hi from some important function!-None-True] PASSED [  5%]
tests/a_module_level_constants/test_config_import_directly.py::test_output_different_systems[only want to do in the cloud-super-cloud.compute-True] PASSED [ 11%]
tests/a_module_level_constants/test_config_import_directly.py::test_output_different_systems[only want to do in the cloud-random.machine-False] PASSED [ 16%]
tests/a_module_level_constants/test_config_import_directly.py::test_output_different_systems[[devbox]-random.machine-True] PASSED [ 22%]
tests/a_module_level_constants/test_config_import_directly.py::test_output_different_systems[secondary-random.machine-True] PASSED [ 27%]
tests/a_module_level_constants/test_config_import_directly.py::test_output_different_systems[[ci]-ci-tron2000.net-True] PASSED [ 33%]
tests/a_module_level_constants/test_config_import_directly.py::test_output_different_systems[[cloud]-ci-tron2000.net-False] PASSED [ 38%]
tests/a_module_level_constants/test_config_import_directly.py::test_output_different_systems[[cloud]-wherever-False] PASSED [ 44%]
tests/a_module_level_constants/test_config_import_module.py::test_output_different_systems[Hi from some important function!-None-True] PASSED [ 50%]
tests/a_module_level_constants/test_config_import_module.py::test_output_different_systems[only want to do in the cloud-super-cloud.compute-True] PASSED [ 55%]
tests/a_module_level_constants/test_config_import_module.py::test_output_different_systems[only want to do in the cloud-random.machine-False] PASSED [ 61%]
tests/a_module_level_constants/test_config_import_module.py::test_output_different_systems[[devbox]-random.machine-True] PASSED [ 66%]
tests/a_module_level_constants/test_config_import_module.py::test_output_different_systems[secondary-random.machine-True] PASSED [ 72%]
tests/a_module_level_constants/test_config_import_module.py::test_output_different_systems[[ci]-ci-tron2000.net-True] PASSED [ 77%]
tests/a_module_level_constants/test_config_import_module.py::test_output_different_systems[[cloud]-ci-tron2000.net-False] PASSED [ 83%]
tests/a_module_level_constants/test_config_import_module.py::test_output_different_systems[[cloud]-wherever-False] PASSED [ 88%]
tests/a_module_level_constants/test_constants_import_directly.py::test_const_on_ci PASSED [ 94%]
tests/a_module_level_constants/test_constants_import_module.py::test_const_on_ci XFAIL [100%]

=========================== short test summary info ============================
XFAIL tests/a_module_level_constants/test_constants_import_module.py::test_const_on_ci
  only works if complete module is imported
======================== 17 passed, 1 xfailed in 0.18s =========================
START a_module_level_constants/the_system.py START
ROOT=/home/ob/do/automagic-configuration
FQDN=delltmp
PLATFORM_NAME=devbox
CI=False
CLOUD=False
DEVBOX=True
END a_module_level_constants/the_system.py END

START a_module_level_constants/__init__.py START
END a_module_level_constants/__init__.py END

START a_module_level_constants/constants.py START
STATIC_INFO=static-info
SOME_PATH=/home/ob/secondary/some
SOME_OTHER_PATH=other
DERIVED_INFO=static-info-derived
DERIVED_PATH=/home/ob/secondary/some/other
END a_module_level_constants/constants.py END

START a_module_level_constants/app_import_module.py START
END a_module_level_constants/app_import_module.py END

FAKE APP! Using app_import_module
Hi from some important function!
[devbox] /home/ob/secondary/some/other
============================= test session starts ==============================
platform linux -- Python 3.8.10, pytest-7.0.1, pluggy-1.0.0 -- .../automagic-configuration/.venv/bin/python
cachedir: .venv/.pytest_cache
rootdir: .../automagic-configuration, configfile: tox.ini
plugins: xonsh-0.11.0
collecting ... collected 9 items

tests/b_namespace_class/test_config.py::test_output_different_systems[Hi from some important function!-None-True] PASSED [ 11%]
tests/b_namespace_class/test_config.py::test_output_different_systems[only want to do in the cloud-super-cloud.compute-True] PASSED [ 22%]
tests/b_namespace_class/test_config.py::test_output_different_systems[only want to do in the cloud-random.machine-False] PASSED [ 33%]
tests/b_namespace_class/test_config.py::test_output_different_systems[[devbox]-random.machine-True] PASSED [ 44%]
tests/b_namespace_class/test_config.py::test_output_different_systems[secondary-random.machine-True] PASSED [ 55%]
tests/b_namespace_class/test_config.py::test_output_different_systems[[ci]-ci-tron2000.net-True] PASSED [ 66%]
tests/b_namespace_class/test_config.py::test_output_different_systems[[cloud]-ci-tron2000.net-False] PASSED [ 77%]
tests/b_namespace_class/test_config.py::test_output_different_systems[[cloud]-wherever-False] PASSED [ 88%]
tests/b_namespace_class/test_constants.py::test_const_on_ci PASSED       [100%]

============================== 9 passed in 0.10s ===============================
START b_namespace_class/the_system.py START
delltmp
type(self)=<class 'b_namespace_class.the_system._RUNS_ON'> - singleton instance (not enforced)
	self.PLATFORM_NAME='devbox'
	self.DEVBOX=True
	self.CI=False
	self.CLOUD=False

END b_namespace_class/the_system.py END

START b_namespace_class/__init__.py START
END b_namespace_class/__init__.py END

START b_namespace_class/constants.py START
type(CONST)=<class 'type'> (not an instance, just the class object)

	CONST.STATIC_INFO='static-info'
	CONST.SOME_PATH=PosixPath('/home/ob/secondary/some')
	CONST.SOME_OTHER_PATH=PosixPath('other')
	CONST.DERIVED_INFO='static-info-derived'
	CONST.DERIVED_PATH=PosixPath('/home/ob/secondary/some/other')

END b_namespace_class/constants.py END

START b_namespace_class/app.py START
END b_namespace_class/app.py END

Hi from some important function!
[devbox] /home/ob/secondary/some/other
============================= test session starts ==============================
platform linux -- Python 3.8.10, pytest-7.0.1, pluggy-1.0.0 -- .../automagic-configuration/.venv/bin/python
cachedir: .venv/.pytest_cache
rootdir: .../automagic-configuration, configfile: tox.ini
plugins: xonsh-0.11.0
collecting ... collected 10 items

tests/c_singleton_instance_with_properties/test_config.py::test_monkeypatch_property_does_not_work PASSED [ 10%]
tests/c_singleton_instance_with_properties/test_config.py::test_output_different_systems[Hi from some important function!-None-True] PASSED [ 20%]
tests/c_singleton_instance_with_properties/test_config.py::test_output_different_systems[only want to do in the cloud-super-cloud.compute-True] PASSED [ 30%]
tests/c_singleton_instance_with_properties/test_config.py::test_output_different_systems[only want to do in the cloud-random.machine-False] PASSED [ 40%]
tests/c_singleton_instance_with_properties/test_config.py::test_output_different_systems[[devbox]-random.machine-True] PASSED [ 50%]
tests/c_singleton_instance_with_properties/test_config.py::test_output_different_systems[secondary-random.machine-True] PASSED [ 60%]
tests/c_singleton_instance_with_properties/test_config.py::test_output_different_systems[[ci]-ci-tron2000.net-True] PASSED [ 70%]
tests/c_singleton_instance_with_properties/test_config.py::test_output_different_systems[[cloud]-ci-tron2000.net-False] PASSED [ 80%]
tests/c_singleton_instance_with_properties/test_config.py::test_output_different_systems[[cloud]-wherever-False] PASSED [ 90%]
tests/c_singleton_instance_with_properties/test_constants.py::test_const_on_ci PASSED [100%]

============================== 10 passed in 0.12s ==============================
START c_singleton_instance_with_properties/the_system.py START

	FQDN='delltmp'
	self.PLATFORM_NAME='devbox'
	self.DEVBOX=True
	self.CI=False
	self.CLOUD=False

END c_singleton_instance_with_properties/the_system.py END

START c_singleton_instance_with_properties/__init__.py START
END c_singleton_instance_with_properties/__init__.py END

START c_singleton_instance_with_properties/constants.py START

	self.STATIC_INFO='static-info'
	self.SOME_PATH=PosixPath('/home/ob/secondary/some')
	self.SOME_OTHER_PATH=PosixPath('other')
	self.DERIVED_INFO='static-info-derived'
	self.DERIVED_PATH=PosixPath('/home/ob/secondary/some/other')

END c_singleton_instance_with_properties/constants.py END

START c_singleton_instance_with_properties/app.py START
END c_singleton_instance_with_properties/app.py END

Hi from some important function!
[devbox] /home/ob/secondary/some/other
============================= test session starts ==============================
platform linux -- Python 3.8.10, pytest-7.0.1, pluggy-1.0.0 -- .../automagic-configuration/.venv/bin/python
cachedir: .venv/.pytest_cache
rootdir: .../automagic-configuration, configfile: tox.ini
plugins: xonsh-0.11.0
collecting ... collected 13 items

tests/d_descriptors_annotations_config/test_config.py::test_output_different_systems[Hi from some important function!-None-True] PASSED [  7%]
tests/d_descriptors_annotations_config/test_config.py::test_output_different_systems[only want to do in the cloud-super-cloud.compute-True] PASSED [ 15%]
tests/d_descriptors_annotations_config/test_config.py::test_output_different_systems[only want to do in the cloud-random.machine-False] PASSED [ 23%]
tests/d_descriptors_annotations_config/test_config.py::test_output_different_systems[[devbox]-random.machine-True] PASSED [ 30%]
tests/d_descriptors_annotations_config/test_config.py::test_output_different_systems[secondary-random.machine-True] PASSED [ 38%]
tests/d_descriptors_annotations_config/test_config.py::test_output_different_systems[[ci]-ci-tron2000.net-True] PASSED [ 46%]
tests/d_descriptors_annotations_config/test_config.py::test_output_different_systems[[cloud]-ci-tron2000.net-False] PASSED [ 53%]
tests/d_descriptors_annotations_config/test_config.py::test_output_different_systems[[cloud]-wherever-False] PASSED [ 61%]
tests/d_descriptors_annotations_config/test_constants.py::test_uninitialized_const_crashes_on_access PASSED [ 69%]
tests/d_descriptors_annotations_config/test_constants.py::test_uninitialized_derived_str_const_crashes_on_access PASSED [ 76%]
tests/d_descriptors_annotations_config/test_constants.py::test_uninitialized_derived_path_const_crashes_on_access PASSED [ 84%]
tests/d_descriptors_annotations_config/test_constants.py::test_initialized_derived_const_works PASSED [ 92%]
tests/d_descriptors_annotations_config/test_constants.py::test_const_on_ci PASSED [100%]

============================== 13 passed in 0.15s ==============================
the_system.py: PREPARING ... 
	FQDN='delltmp'
	RUNS_ON.PLATFORM_NAME='devbox'
	RUNS_ON.DEVBOX=True
	RUNS_ON.CI=False
	RUNS_ON.CLOUD=False

START d_descriptors_annotations_config/__init__.py START
END d_descriptors_annotations_config/__init__.py END

START d_descriptors_annotations_config/constants.py START

	hasattr(CONST, '_BASE_PATH')=False
	hasattr(CONST, 'STATIC_INFO')=False
	hasattr(CONST, 'SOME_PATH')=False
	CONST.SOME_OTHER_PATH=PosixPath('other')
	hasattr(CONST, 'DERIVED_INFO')=False
	hasattr(CONST, 'DERIVED_PATH')=False

WARNING: constants are not initialized yet!

END d_descriptors_annotations_config/constants.py END

START d_descriptors_annotations_config/app.py START
END d_descriptors_annotations_config/app.py END

INITIALIZING CONSTANTS FROM
	(path, info)=('/home/ob/secondary', 'static-info') ... initialize_app:

	constants.CONST.STATIC_INFO='static-info'
	constants.CONST.SOME_PATH=PosixPath('/home/ob/secondary')
	constants.CONST.SOME_OTHER_PATH=PosixPath('other')
	constants.CONST.DERIVED_INFO='static-info-derived-value'
	constants.CONST.DERIVED_PATH=PosixPath('/home/ob/secondary/other/somefile')

Hi from some important function!
[devbox] /home/ob/secondary/other/somefile
============================= test session starts ==============================
platform linux -- Python 3.8.10, pytest-7.0.1, pluggy-1.0.0 -- .../automagic-configuration/.venv/bin/python
cachedir: .venv/.pytest_cache
rootdir: .../automagic-configuration, configfile: tox.ini
plugins: xonsh-0.11.0
collecting ... collected 10 items

tests/e_freezable_descriptor_based_singleton/test_config.py::test_monkeypatch_also_works_now PASSED [ 10%]
tests/e_freezable_descriptor_based_singleton/test_config.py::test_output_different_systems[Hi from some important function!-None-True] PASSED [ 20%]
tests/e_freezable_descriptor_based_singleton/test_config.py::test_output_different_systems[only want to do in the cloud-super-cloud.compute-True] PASSED [ 30%]
tests/e_freezable_descriptor_based_singleton/test_config.py::test_output_different_systems[only want to do in the cloud-random.machine-False] PASSED [ 40%]
tests/e_freezable_descriptor_based_singleton/test_config.py::test_output_different_systems[[devbox]-random.machine-True] PASSED [ 50%]
tests/e_freezable_descriptor_based_singleton/test_config.py::test_output_different_systems[secondary-random.machine-True] PASSED [ 60%]
tests/e_freezable_descriptor_based_singleton/test_config.py::test_output_different_systems[[ci]-ci-tron2000.net-True] PASSED [ 70%]
tests/e_freezable_descriptor_based_singleton/test_config.py::test_output_different_systems[[cloud]-ci-tron2000.net-False] PASSED [ 80%]
tests/e_freezable_descriptor_based_singleton/test_config.py::test_output_different_systems[[cloud]-wherever-False] PASSED [ 90%]
tests/e_freezable_descriptor_based_singleton/test_constants.py::test_const_on_ci PASSED [100%]

============================== 10 passed in 0.05s ==============================
START e_freezable_descriptor_based_singleton/__init__.py START
START e_freezable_descriptor_based_singleton/the_system.py START
[FREEZE] instance=_RUNS_ON.self.name='PLATFORM_NAME' => 'attr='devbox''
[FREEZE] instance=_RUNS_ON.self.name='DEVBOX' => 'attr=True'
[FREEZE] instance=_RUNS_ON.self.name='CI' => 'attr=False'
[FREEZE] instance=_RUNS_ON.self.name='CLOUD' => 'attr=False'

	FQDN='delltmp'
	RUNS_ON.PLATFORM_NAME='devbox'
	RUNS_ON.DEVBOX=True
	RUNS_ON.CI=False
	RUNS_ON.CLOUD=False

END e_freezable_descriptor_based_singleton/the_system.py END

END e_freezable_descriptor_based_singleton/__init__.py END

START e_freezable_descriptor_based_singleton/constants.py START
[FREEZE] instance=_CONST.self.name='_BASE_PATH' => 'attr=PosixPath('/home/ob/secondary')'
[FREEZE] instance=_CONST.self.name='SOME_PATH' => 'attr=PosixPath('/home/ob/secondary/some')'
[FREEZE] instance=_CONST.self.name='DERIVED_INFO' => 'attr='static-info-derived''
[FREEZE] instance=_CONST.self.name='DERIVED_PATH' => 'attr=PosixPath('/home/ob/secondary/some/other')'

	CONST.STATIC_INFO='static-info'
	CONST.SOME_PATH=PosixPath('/home/ob/secondary/some')
	CONST.SOME_OTHER_PATH=PosixPath('other')
	CONST.DERIVED_INFO='static-info-derived'
	CONST.DERIVED_PATH=PosixPath('/home/ob/secondary/some/other')

END e_freezable_descriptor_based_singleton/constants.py END

START e_freezable_descriptor_based_singleton/app.py START
END e_freezable_descriptor_based_singleton/app.py END

Hi from some important function!
[devbox] /home/ob/secondary/some/other
START e_freezable_descriptor_based_singleton/__init__.py START
START e_freezable_descriptor_based_singleton/the_system.py START
[FREEZE] instance=_RUNS_ON.self.name='PLATFORM_NAME' => 'attr='devbox''
[FREEZE] instance=_RUNS_ON.self.name='DEVBOX' => 'attr=True'
[FREEZE] instance=_RUNS_ON.self.name='CI' => 'attr=False'
[FREEZE] instance=_RUNS_ON.self.name='CLOUD' => 'attr=False'

	FQDN='delltmp'
	RUNS_ON.PLATFORM_NAME='devbox'
	RUNS_ON.DEVBOX=True
	RUNS_ON.CI=False
	RUNS_ON.CLOUD=False

END e_freezable_descriptor_based_singleton/the_system.py END

END e_freezable_descriptor_based_singleton/__init__.py END

ROCKET SCIENCE CONSTANT DEMO ...

new instance of FOO=_FOO - FOO.PROPERTY='PROPERTY: 117894086653481'
sleeping a bit ...
FOO.PROPERTY='PROPERTY: 117894586825790' => new timestamp on every call
trying to set FOO.PROPERTY ...
[IGNORE] e=AttributeError("can't set attribute") => property is read only without a setter
trying to set delete FOO.PROPERTY ...
[IGNORE] e=AttributeError("can't delete attribute") => property is not deletable without deleter
########################################

a non-data descriptor does not prevent an object attribute to be set ...
setting FOO.FROZEN
completely circumvented calculation: I was overwritten
########################################

deleting FOO.FROZEN ...
[FREEZE] instance=_FOO.self.name='FROZEN' => 'attr='FROZEN: 117894586855012''
FOO.FROZEN='FROZEN: 117894586855012' => attribute only shadowed descriptor
sleep a bit ...
FOO.FROZEN='FROZEN: 117894586855012' => still frozen
########################################

setting FOO.FROZEN again ...
FOO.FROZEN='FROZEN: I was overwritten again'
sleep a bit ...
thaw the whole object ...
[THAW] name='FROZEN' on self=_FOO
[FREEZE] instance=_FOO.self.name='FROZEN' => 'attr='FROZEN: 117895588183960''
FOO.FROZEN='FROZEN: 117895588183960'
sleep a bit ...
FOO.FROZEN='FROZEN: 117895588183960'
___________________________________ summary ____________________________________
  black: commands succeeded
  a: commands succeeded
  b: commands succeeded
  c: commands succeeded
  d: commands succeeded
  e: commands succeeded
  congratulations :)
```

Enjoy!

--

# Some more idle thoughts on this topic

(or: do not write your internal software, as if it would be Open Source Software, unless it really is)

## Intelligent restriction over flexible configuration

Over the years there have been many articles in the vein of [this one](https://ben.balter.com/2012/06/26/why-you-should-always-write-software-as-open-source/), basically telling you to write your internal software as if it were Open Source. There are definitely many aspects of Open Source Software and the way how it is developed that are worthy of emulating (collaboration, code review, automation, documentation, etc.). But some aspects are decidedly not. As everywhere: context matters. Ignoring that might lead to the shoehorning of concepts into internal software which might hurt more than they might help. One of the main culprits here is by introducing unnecessary complexity. Unnecessary complexity is VERY BAD(TM), because there is always already more than enough necessary complexity to go around for a life time.

## Coding by convention

A lot of good coding practices that I learned over the years can be subsumed under the question: "How can we identify and reduce unnecessary complexity in a real world system"?. With "real world" system, I mean something that has grown and flourished over the course of several years already and has seen its share of different developers abd different approaches, ideas, paradigms ... the lot. There is usually a lot of unnecessary complexity hidden and it needs to be revealed and then eradicated. I have no simple recipe for how to that and I won't go into all the aspects of it, but two particular things I want to talk about here. I think they can be already 80% of the way to giving guidance in moving a code base towards less unnecessary complexity.

One of them is ["convention over configuration"](https://en.wikipedia.org/wiki/Convention_over_configuration) a.k.a. "coding by convention". The simple idea of making a lot of configuration superfluous by having clear conventions about naming, locations and approaches can get you a lot way in an environment, where a clearly defined goup of people control the complete lifecycle of a software system. 

In the different projects I was working on over the years there was often some kind of pattern emerging that there is a common base theme of some kind of "bread and butter functionality" that does the heavy lifting around, development, testing, deployment and common functionality and then what differs between different applications. Those are all kinds of plugin systems in one way or the other. The trick is to make the common base robust and powerful enough that building the business logic on top of that becomes easy and provides a good fundament to grow on. 

One hallmark of great project is that it found the sweet spot between configurability and sane defaults, that make 90% of the users happy for their simple use cases and gives the 10% of power users enough flexibility to cover their more exotic use cases.

The more widespread the user base of a system is and the more diverse the use cases, the harder the software is to maintain and the larger the danger that changes and extensions cause unforeseen side effects in stranger corners of the system, where functionality was tacked on to cover interesting use cases or system configurations / versions. The result is that those program often have a large number of configuration options, command line flags, etc. just look at some widely used open source projects and their configuration options and you know what I mean. Another result is that they often have a large number of bugs and regressions, even if they have a good test coverage. If you want to get a feeling for what "combinatoric explosion" means just try to work on such a system for a while.

[![obligatory xkcd](https://imgs.xkcd.com/comics/workflow.png )](https://xkcd.com/1172/)

**(obligatory xkcd)**

## Ok - what now?

Writing internal software in a small team, is a completely different situation and every additional option or possibility to configure something in a different way should be seen as a large burden that increases the number of different code paths significantly and also increases the mental burden of the programmer when they need to make changes or extensions.

The only solution to not be falling into the trap of unnecessary complexitiy and fall victim to unmanagable combinatoric explosions is the opposite of what OSS projects do: 

**Restriction and Reduction**

* Restriction of the number of environments a software needs to be able to run on
* Reduction of the number of ways as system can be configured to

For internal systems this is an achievable and due to above thought - desirable goal. A team knows exactly where a system needs to run and it should focus on that and only that.

The approach to configuration in internal systems should therefore be fundamentally different from what we might be used to in open source projects.

First and foremost the question of what kind of format the configuration should be in:

For internal systems it is often possible to get away without configuration in the classic sense and infer all configuration from hints in the environment. The configuration is then different depending on where the system runs. The configuration is done entirely in Python and lives in (namespaced) constants (yes, I know there are no constants in Python ... you know what I mean :)). 

The example used here is using the domain of a system to determine if we are running on a developer box, on CI or on some compute node in the cloud. The module `the_system` contains "constants" which can be imported and accessed wherever they are needed to e.g. trigger different behaviour depending on these values.

E.g. the example shows a function that prints a certain line only, when running in the cloud.

The accompanying tests show how to simulate different runtime environments by manipulating these "constants" via pytests monkeypatch fixture.
