from pathlib import Path


# cannot import the_system here yet, circular import problems ...
# see what happens, when you uncomment it
# from e_freezable_descriptor_based_singleton import the_system
print(f"START {Path(__file__).relative_to(Path(__file__).parents[1])} START")


class ROCKET_SCIENCE_MIXIN:
    """Provide descriptor driven, controllable data caching, simulating constants

    Likely in a horribly wrong way and not protecting from accidental mutation.

    WARNING only looks at public and private attributes (__ explicitly ignored).
    """

    def __str__(self):
        return f"{self.__class__.__name__}"

    __repr__ = __str__

    class _rsc_freeze:
        """Use as a decorator to freeze a property into a normal attr on access.

        This is a non-data descriptor, therefore the precedence is lower than
        an instance attribute. When it is accessed first, the attribute will be calculated
        and set as an instance attribute therefore shadowing the descriptor.
        """

        def __init__(self, func):
            self.name = func.__name__
            self.func = func

        def __get__(self, instance, owner):
            attr = self.func(instance)
            print(f"[FREEZE] {instance=}.{self.name=} => '{attr=}'")
            setattr(instance, self.name, attr)
            return attr

    def _rsc_thaw(self):
        """Delete all instance attributes that shadow a class attribute.

        Forces re-freezing of frozen properties and reveals overriden class attrs.
        """
        shadowed = self._rsc_instance_attrs.keys()
        for name in shadowed:
            print(f"[THAW] {name=} on {self=}")
            delattr(self, name)

    @property
    def _rsc_properties(self):
        return {
            k: getattr(self, k)
            for k, v in self.__class__.__dict__.items()
            if not k.startswith("__") and isinstance(v, property)
        }

    @property
    def _rsc_instance_attrs(self):
        return {
            k: v
            for k, v in self.__dict__.items()
            if not k.startswith("__") and not callable(v)
        }


# I can here though ...
from e_freezable_descriptor_based_singleton import the_system


print(f"END {Path(__file__).relative_to(the_system.ROOT)} END\n")
