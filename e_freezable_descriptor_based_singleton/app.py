from pathlib import Path

from e_freezable_descriptor_based_singleton import constants, the_system

print(f"START {Path(__file__).relative_to(the_system.ROOT)} START")


def some_important_function():
    print("Hi from some important function!")
    if the_system.RUNS_ON.CLOUD:
        print("doing something I only want to do in the cloud ...")
    else:
        print(f"[{the_system.RUNS_ON.PLATFORM_NAME}] {constants.CONST.DERIVED_PATH}")


if __name__ == "__prez__":
    some_important_function()


print(f"END {Path(__file__).relative_to(the_system.ROOT)} END\n")
