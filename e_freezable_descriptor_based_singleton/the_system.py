import socket
from pathlib import Path

from e_freezable_descriptor_based_singleton import ROCKET_SCIENCE_MIXIN

ROOT = Path(__file__).parents[1]

print(f"START {Path(__file__).relative_to(ROOT)} START")

FQDN = socket.getfqdn()


class _RUNS_ON(ROCKET_SCIENCE_MIXIN):
    _NAME_DEVBOX = "devbox"
    _NAME_CI = "ci"
    _NAME_CLOUD = "cloud"
    _CRITERION_CLOUD_HOST = "super-cloud.compute"
    _CRITERION_CI_HOST = "ci-tron2000.net"

    @ROCKET_SCIENCE_MIXIN._rsc_freeze
    def PLATFORM_NAME(self):
        return self._determine_platform()

    @ROCKET_SCIENCE_MIXIN._rsc_freeze
    def CLOUD(self):
        return self.PLATFORM_NAME == self._NAME_CLOUD

    @ROCKET_SCIENCE_MIXIN._rsc_freeze
    def CI(self):
        return self.PLATFORM_NAME == self._NAME_CI

    @ROCKET_SCIENCE_MIXIN._rsc_freeze
    def DEVBOX(self):
        return self.PLATFORM_NAME == self._NAME_DEVBOX

    @classmethod
    def _determine_platform(cls):
        if cls._CRITERION_CLOUD_HOST in FQDN:
            return cls._NAME_CLOUD

        if cls._CRITERION_CI_HOST in FQDN:
            return cls._NAME_CI

        return cls._NAME_DEVBOX


RUNS_ON = _RUNS_ON()

print(
    f"\n\t{FQDN=}\n"
    f"\t{RUNS_ON.PLATFORM_NAME=}\n"
    f"\t{RUNS_ON.DEVBOX=}\n"
    f"\t{RUNS_ON.CI=}\n"
    f"\t{RUNS_ON.CLOUD=}\n"
)

print(f"END {Path(__file__).relative_to(ROOT)} END\n")
