import time

from e_freezable_descriptor_based_singleton import ROCKET_SCIENCE_MIXIN


class _FOO(ROCKET_SCIENCE_MIXIN):
    @property
    def PROPERTY(self):
        return f"PROPERTY: {time.monotonic_ns()}"

    @ROCKET_SCIENCE_MIXIN._rsc_freeze
    def FROZEN(self):
        return f"FROZEN: {time.monotonic_ns()}"


print("ROCKET SCIENCE CONSTANT DEMO ...", end="\n\n")
FOO = _FOO()

############################ PROPERTIES ################################
print(f"new instance of {FOO=} - {FOO.PROPERTY=}")
print("sleeping a bit ...")
time.sleep(0.5)
print(f"{FOO.PROPERTY=} => new timestamp on every call")
try:
    print("trying to set FOO.PROPERTY ...")
    FOO.PROPERTY = 1  # noqa
except AttributeError as e:
    print(f"[IGNORE] {e=} => property is read only without a setter")

try:
    print("trying to set delete FOO.PROPERTY ...")
    del FOO.PROPERTY  # noqa
except AttributeError as e:
    print(f"[IGNORE] {e=} => property is not deletable without deleter")

print("#" * 40, end="\n\n")


######################### NORMAL ATTRIBUTES ##############################

print("a non-data descriptor does not prevent an object attribute to be set ...")
print("setting FOO.FROZEN")
FOO.FROZEN = "I was overwritten"
print(f"completely circumvented calculation: {FOO.FROZEN}")

print("#" * 40, end="\n\n")

print("deleting FOO.FROZEN ...")
del FOO.FROZEN
print(f"{FOO.FROZEN=} => attribute only shadowed descriptor")
print("sleep a bit ...")
time.sleep(0.5)
print(f"{FOO.FROZEN=} => still frozen")

print("#" * 40, end="\n\n")

print("setting FOO.FROZEN again ...")
FOO.FROZEN = "FROZEN: I was overwritten again"
print(f"{FOO.FROZEN=}")
print("sleep a bit ...")
time.sleep(0.5)
print("thaw the whole object ...")
FOO._rsc_thaw()
print(f"{FOO.FROZEN=}")
print("sleep a bit ...")
time.sleep(0.5)
print(f"{FOO.FROZEN=}")
