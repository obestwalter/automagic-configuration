import os
import tempfile
from pathlib import Path

from e_freezable_descriptor_based_singleton import ROCKET_SCIENCE_MIXIN, the_system

print(f"START {Path(__file__).relative_to(the_system.ROOT)} START")


class _CONST(ROCKET_SCIENCE_MIXIN):
    STATIC_INFO = "static-info"
    SOME_OTHER_PATH = Path("other")

    @ROCKET_SCIENCE_MIXIN._rsc_freeze
    def _BASE_PATH(self):
        if the_system.RUNS_ON.CI:
            _path = os.getenv("SOME_ENV_VAR_SET_BY_CI")
            _path = Path(_path, "build-tmp") if _path else Path(tempfile.mkdtemp())
            return _path
        else:
            _path = Path("/primary")
            if _path.exists() and os.access(str(_path), os.W_OK | os.X_OK):
                return _path

            _path = Path.home() / "secondary"
            _path.mkdir(exist_ok=True)
            return _path

    @ROCKET_SCIENCE_MIXIN._rsc_freeze
    def SOME_PATH(self):
        return self._BASE_PATH / "some"

    @ROCKET_SCIENCE_MIXIN._rsc_freeze
    def DERIVED_INFO(self):
        return f"{self.STATIC_INFO}-derived"

    @ROCKET_SCIENCE_MIXIN._rsc_freeze
    def DERIVED_PATH(self):
        return self.SOME_PATH / self.SOME_OTHER_PATH


CONST = _CONST()

print(
    f"\n\t{CONST.STATIC_INFO=}\n"
    f"\t{CONST.SOME_PATH=}\n"
    f"\t{CONST.SOME_OTHER_PATH=}\n"
    f"\t{CONST.DERIVED_INFO=}\n"
    f"\t{CONST.DERIVED_PATH=}\n"
)

print(f"END {Path(__file__).relative_to(the_system.ROOT)} END\n")
