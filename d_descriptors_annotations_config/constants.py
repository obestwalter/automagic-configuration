from pathlib import Path

from d_descriptors_annotations_config import the_system

print(f"START {Path(__file__).relative_to(the_system.ROOT)} START")


class _DerivedStrConstant:
    def __init__(self, constructor: str):
        """Expects a string with formatting syntax to be populated."""
        self.constructor = constructor

    def __set_name__(self, owner, name):
        self.name = name

    def __get__(self, instance, owner) -> str:
        try:
            return self.constructor.format_map(instance.__dict__)
        except KeyError:
            # simulate correct behaviour - as derived constant does not "exist" yet
            raise AttributeError(
                f"'{owner}' object has no attribute {self.name}"
            ) from None


class _DerivedPathConstant:
    def __init__(self, *parts):
        """Expects class attribute names and strings to construct the path from."""
        self.parts = parts

    def __set_name__(self, owner, name):
        self.name = name

    def __get__(self, instance, owner) -> Path:
        for part in self.parts:
            if part in instance.__annotations__ and not hasattr(instance, part):
                raise AttributeError(
                    f"'{owner}' object has no attribute {self.name}"
                ) from None

        return Path(*[getattr(instance, e, e) for e in self.parts])


class _CONST:
    _BASE_PATH: Path
    STATIC_INFO: str  # just the type annotation - it's there - but not there ...
    SOME_PATH: Path
    SOME_OTHER_PATH = Path("other")
    DERIVED_INFO = _DerivedStrConstant("{STATIC_INFO}-derived-value")
    DERIVED_PATH = _DerivedPathConstant("SOME_PATH", "SOME_OTHER_PATH", "somefile")


CONST = _CONST()

print(
    f"\n\t{hasattr(CONST, '_BASE_PATH')=}\n"
    f"\t{hasattr(CONST, 'STATIC_INFO')=}\n"
    f"\t{hasattr(CONST, 'SOME_PATH')=}\n"
    f"\t{CONST.SOME_OTHER_PATH=}\n"
    f"\t{hasattr(CONST, 'DERIVED_INFO')=}\n"  # hasattr just catches AttributeError
    f"\t{hasattr(CONST, 'DERIVED_PATH')=}\n"
)

print("WARNING: constants are not initialized yet!\n")

print(f"END {Path(__file__).relative_to(the_system.ROOT)} END\n")
