import socket
from pathlib import Path

ROOT = Path(__file__).parents[1]

print(f"{Path(__file__).name}: PREPARING ...", end=" ")

FQDN = socket.getfqdn()


class _RUNS_ON:
    _NAME_DEVBOX = "devbox"
    _NAME_CI = "ci"
    _NAME_CLOUD = "cloud"
    _CRITERION_CLOUD_HOST = "super-cloud.compute"
    _CRITERION_CI_HOST = "ci-tron2000.net"

    @property
    def PLATFORM_NAME(self):
        return self._determine_platform()

    @property
    def CLOUD(self):
        return self.PLATFORM_NAME == self._NAME_CLOUD

    @property
    def CI(self):
        return self.PLATFORM_NAME == self._NAME_CI

    @property
    def DEVBOX(self):
        return self.PLATFORM_NAME == self._NAME_DEVBOX

    @classmethod
    def _determine_platform(cls):
        if cls._CRITERION_CLOUD_HOST in FQDN:
            return cls._NAME_CLOUD

        if cls._CRITERION_CI_HOST in FQDN:
            return cls._NAME_CI

        return cls._NAME_DEVBOX


RUNS_ON = _RUNS_ON()

print(
    f"\n\t{FQDN=}\n"
    f"\t{RUNS_ON.PLATFORM_NAME=}\n"
    f"\t{RUNS_ON.DEVBOX=}\n"
    f"\t{RUNS_ON.CI=}\n"
    f"\t{RUNS_ON.CLOUD=}\n"
)
