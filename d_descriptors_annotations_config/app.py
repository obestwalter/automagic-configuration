from pathlib import Path

from d_descriptors_annotations_config import constants, the_system

# import that way -see test_initialized_derived_const_works() fail
# from d_descriptors_annotations_config.constants import CONST

print(f"START {Path(__file__).relative_to(the_system.ROOT)} START")


def main():
    initialize_app()
    some_important_function()


def initialize_app(path=str(Path.home() / "secondary"), info="static-info"):
    print(f"INITIALIZING CONSTANTS FROM\n\t{(path, info)=} ...", end=" ")
    constants.CONST._BASE_PATH = path
    constants.CONST.SOME_PATH = Path(path)
    constants.CONST.STATIC_INFO = info
    print(
        "initialize_app:\n"
        f"\n\t{constants.CONST.STATIC_INFO=}\n"
        f"\t{constants.CONST.SOME_PATH=}\n"
        f"\t{constants.CONST.SOME_OTHER_PATH=}\n"
        f"\t{constants.CONST.DERIVED_INFO=}\n"
        f"\t{constants.CONST.DERIVED_PATH=}\n"
    )


def some_important_function():
    print("Hi from some important function!")
    if the_system.RUNS_ON.CLOUD:
        print("doing something I only want to do in the cloud ...")
    else:
        print(f"[{the_system.RUNS_ON.PLATFORM_NAME}] {constants.CONST.DERIVED_PATH}")


if __name__ == "__main__":
    initialize_app()
    some_important_function()

print(f"END {Path(__file__).relative_to(the_system.ROOT)} END\n")
