import os
import tempfile
from pathlib import Path

from a_module_level_constants import the_system

print(f"START {Path(__file__).relative_to(the_system.ROOT)} START")

STATIC_INFO = "static-info"
if the_system.CI:
    _path = os.getenv("SOME_ENV_VAR_SET_BY_CI")
    _path = Path(_path, "build-tmp") if _path else Path(tempfile.mkdtemp())
    _BASE_PATH = _path
else:
    _path = Path("/primary")
    if _path.exists() and os.access(str(_path), os.W_OK | os.X_OK):
        _BASE_PATH = _path
    else:
        _path = Path.home() / "secondary"
        _path.mkdir(exist_ok=True)
        _BASE_PATH = _path
SOME_PATH = _BASE_PATH / "some"
SOME_OTHER_PATH = Path("other")
DERIVED_INFO = f"{STATIC_INFO}-derived"
DERIVED_PATH = SOME_PATH / SOME_OTHER_PATH


for name, obj in globals().copy().items():
    if not name.isupper() or name.startswith("_"):
        continue
    print(f"{name}={obj}")

print(f"END {Path(__file__).relative_to(the_system.ROOT)} END\n")
