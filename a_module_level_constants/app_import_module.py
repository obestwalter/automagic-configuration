from pathlib import Path

from a_module_level_constants import constants, the_system

print(f"START {Path(__file__).relative_to(the_system.ROOT)} START")


def some_important_function():
    print("Hi from some important function!")
    if the_system.CLOUD:
        print("doing something I only want to do in the cloud ...")
    else:
        print(f"[{the_system.PLATFORM_NAME}] {constants.DERIVED_PATH}")


if __name__ == "__main__":
    some_important_function()


print(f"END {Path(__file__).relative_to(the_system.ROOT)} END\n")
