from pathlib import Path

from a_module_level_constants import the_system

print(f"START {Path(__file__).relative_to(the_system.ROOT)} START")
print(f"END {Path(__file__).relative_to(the_system.ROOT)} END\n")
