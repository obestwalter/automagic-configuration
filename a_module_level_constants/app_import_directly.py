from pathlib import Path

from a_module_level_constants.constants import DERIVED_PATH
from a_module_level_constants.the_system import ROOT, CLOUD, PLATFORM_NAME

print(f"START {Path(__file__).relative_to(ROOT)} START")


def some_important_function():
    print("Hi from some important function!")
    if CLOUD:
        print("doing something I only want to do in the cloud ...")
    else:
        print(f"[{PLATFORM_NAME}] {DERIVED_PATH}")


if __name__ == "__main__":
    some_important_function()


print(f"END {Path(__file__).relative_to(ROOT)} END\n")
