import socket
from pathlib import Path

ROOT = Path(__file__).parents[1]

print(f"START {Path(__file__).relative_to(ROOT)} START")

FQDN = socket.getfqdn()

_NAME_DEVBOX = "devbox"
_NAME_CI = "ci"
_NAME_CLOUD = "cloud"
_CRITERION_CLOUD_HOST = "super-cloud.compute"
_CRITERION_CI_HOST = "ci-tron2000.net"


def determine_platform(fqdn):
    if _CRITERION_CLOUD_HOST in fqdn:
        return _NAME_CLOUD

    if _CRITERION_CI_HOST in fqdn:
        return _NAME_CI

    return _NAME_DEVBOX


PLATFORM_NAME = determine_platform(FQDN)
CI = PLATFORM_NAME == _NAME_CI
CLOUD = PLATFORM_NAME == _NAME_CLOUD
DEVBOX = not CLOUD and not CI


for name, obj in globals().copy().items():
    if not name.isupper() or name.startswith("_"):
        continue
    print(f"{name}={obj}")


print(f"END {Path(__file__).relative_to(ROOT)} END\n")
