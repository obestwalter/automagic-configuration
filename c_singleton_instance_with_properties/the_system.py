import socket
from pathlib import Path

ROOT = Path(__file__).parents[1]

print(f"START {Path(__file__).relative_to(ROOT)} START")

FQDN = socket.getfqdn()


class _RUNS_ON:
    _NAME_DEVBOX = "devbox"
    _NAME_CI = "ci"
    _NAME_CLOUD = "cloud"
    _CRITERION_CLOUD_HOST = "super-cloud.compute"
    _CRITERION_CI_HOST = "ci-tron2000.net"

    def __str__(self):
        return (
            f"\t{self.PLATFORM_NAME=}\n"
            f"\t{self.DEVBOX=}\n"
            f"\t{self.CI=}\n"
            f"\t{self.CLOUD=}\n"
        )

    __repr__ = __str__

    @property
    def PLATFORM_NAME(self):
        return self._determine_platform()

    @property
    def CLOUD(self):
        return self.PLATFORM_NAME == self._NAME_CLOUD

    @property
    def CI(self):
        return self.PLATFORM_NAME == self._NAME_CI

    @property
    def DEVBOX(self):
        return self.PLATFORM_NAME == self._NAME_DEVBOX

    @classmethod
    def _determine_platform(cls):
        if cls._CRITERION_CLOUD_HOST in FQDN:
            return cls._NAME_CLOUD

        if cls._CRITERION_CI_HOST in FQDN:
            return cls._NAME_CI

        return cls._NAME_DEVBOX


RUNS_ON = _RUNS_ON()
print(f"\n\t{FQDN=}")
print(RUNS_ON)
print(f"END {Path(__file__).relative_to(ROOT)} END\n")
