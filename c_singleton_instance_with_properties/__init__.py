from pathlib import Path

from c_singleton_instance_with_properties import the_system

print(f"START {Path(__file__).relative_to(the_system.ROOT)} START")
print(f"END {Path(__file__).relative_to(the_system.ROOT)} END\n")
