import os
import tempfile
from pathlib import Path

from c_singleton_instance_with_properties import the_system

print(f"START {Path(__file__).relative_to(the_system.ROOT)} START")


class _CONST:
    STATIC_INFO = "static-info"
    SOME_OTHER_PATH = Path("other")

    def __str__(self):
        return (
            f"\n\t{self.STATIC_INFO=}\n"
            f"\t{self.SOME_PATH=}\n"
            f"\t{self.SOME_OTHER_PATH=}\n"
            f"\t{self.DERIVED_INFO=}\n"
            f"\t{self.DERIVED_PATH=}\n"
        )

    __repr__ = __str__

    @property
    def _BASE_PATH(self):
        if the_system.RUNS_ON.CI:
            _path = os.getenv("SOME_ENV_VAR_SET_BY_CI")
            _path = Path(_path, "build-tmp") if _path else Path(tempfile.mkdtemp())
            return _path
        else:
            _path = Path("/primary")
            if _path.exists() and os.access(str(_path), os.W_OK | os.X_OK):
                return _path

            _path = Path.home() / "secondary"
            _path.mkdir(exist_ok=True)
            return _path

    @property
    def SOME_PATH(self):
        return self._BASE_PATH / "some"

    @property
    def DERIVED_INFO(self):
        return f"{self.STATIC_INFO}-derived"

    @property
    def DERIVED_PATH(self):
        return self.SOME_PATH / self.SOME_OTHER_PATH


CONST = _CONST()
print(CONST)
print(f"END {Path(__file__).relative_to(the_system.ROOT)} END\n")
