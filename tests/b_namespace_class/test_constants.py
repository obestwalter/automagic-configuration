from pathlib import Path

from b_namespace_class import constants, the_system
from b_namespace_class.app import some_important_function


def test_const_on_ci(monkeypatch, capsys):
    monkeypatch.setattr(
        the_system, "FQDN", f"extra.{the_system.RUNS_ON._CRITERION_CI_HOST}"
    )
    monkeypatch.setattr(the_system, "RUNS_ON", the_system._RUNS_ON())

    # the patching above isn't enough
    # it is necessary to create a horrible mock class
    class _MOCK_CONST:
        STATIC_INFO = "static-info"
        _BASE_PATH = Path("/some/ci/path", "build-tmp")
        SOME_PATH = _BASE_PATH / "some"
        SOME_OTHER_PATH = Path("other")
        DERIVED_INFO = f"{STATIC_INFO}-derived"
        DERIVED_PATH = SOME_PATH / SOME_OTHER_PATH

    monkeypatch.setattr(constants, "CONST", _MOCK_CONST)
    some_important_function()
    stdout = capsys.readouterr().out
    assert "[ci]" in stdout
    assert str(Path("/some/ci/path/build-tmp/some/other")) in stdout
