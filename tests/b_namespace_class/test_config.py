import pytest

from b_namespace_class import the_system
from b_namespace_class.app import some_important_function


@pytest.mark.parametrize(
    "text, fqdn, found_in_stdout",
    (
        ("Hi from some important function!", None, True),
        (
            "only want to do in the cloud",
            the_system._RUNS_ON._CRITERION_CLOUD_HOST,
            True,
        ),
        ("only want to do in the cloud", "random.machine", False),
        ("[devbox]", "random.machine", True),
        ("secondary", "random.machine", True),
        ("[ci]", the_system._RUNS_ON._CRITERION_CI_HOST, True),
        ("[cloud]", the_system._RUNS_ON._CRITERION_CI_HOST, False),
        ("[cloud]", "wherever", False),
    ),
)
def test_output_different_systems(monkeypatch, capsys, text, fqdn, found_in_stdout):
    if fqdn:
        # Patch the underlying constant and then replace the complete instance with a
        # new one. This is the "right" way to do it with a statically initialized object
        # otherwise "impossible" system configurations would be possible.
        monkeypatch.setattr(the_system, "FQDN", f"extra.{fqdn}")
        monkeypatch.setattr(the_system, "RUNS_ON", the_system._RUNS_ON())
    some_important_function()
    assert (text in capsys.readouterr().out) == found_in_stdout
