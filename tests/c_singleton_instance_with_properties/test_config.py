import pytest

from c_singleton_instance_with_properties import the_system
from c_singleton_instance_with_properties.app import some_important_function


def test_monkeypatch_property_does_not_work(monkeypatch):
    """
    read-only properties are really read-only and can't be monkeypatched
    easily (everything is possible, but the pytest fixture can't do it)
    it can't even be demonstrated with a green test easily, as the failed
    monkeypatch fails again, when trying to undo it in the teardown.
    """
    # uncomment to see the test pass, but the teardown fail :)
    # with pytest.raises(AttributeError):
    #     monkeypatch.setattr(the_system.RUNS_ON, "DEVBOX", True)


@pytest.mark.parametrize(
    "text, system, expectation",
    (
        ("Hi from some important function!", None, True),
        (
            "only want to do in the cloud",
            the_system._RUNS_ON._CRITERION_CLOUD_HOST,
            True,
        ),
        ("only want to do in the cloud", "random.machine", False),
        ("[devbox]", "random.machine", True),
        ("secondary", "random.machine", True),
        ("[ci]", the_system._RUNS_ON._CRITERION_CI_HOST, True),
        ("[cloud]", the_system._RUNS_ON._CRITERION_CI_HOST, False),
        ("[cloud]", "wherever", False),
    ),
)
def test_output_different_systems(monkeypatch, capsys, text, system, expectation):
    if system:
        # patching only necessary for basic constant now
        monkeypatch.setattr(the_system, "FQDN", f"extra.{system}")
    some_important_function()
    assert (text in capsys.readouterr().out) == expectation
