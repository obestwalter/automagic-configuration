from pathlib import Path

import pytest

from e_freezable_descriptor_based_singleton import the_system
from e_freezable_descriptor_based_singleton.app import some_important_function


@pytest.mark.usefixtures("rsc_thaw_all")
def test_const_on_ci(monkeypatch, capsys):
    # just patching the underlying fundamentals again, the rest is derived
    monkeypatch.setattr(
        the_system, "FQDN", f"extra.{the_system.RUNS_ON._CRITERION_CI_HOST}"
    )
    monkeypatch.setenv("SOME_ENV_VAR_SET_BY_CI", "/some/ci/path")
    some_important_function()
    stdout = capsys.readouterr().out
    assert "[ci]" in stdout
    assert str(Path("/some/ci/path/build-tmp/some/other")) in stdout
