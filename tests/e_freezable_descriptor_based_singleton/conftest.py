import pytest

from e_freezable_descriptor_based_singleton import the_system, constants


@pytest.fixture
def rsc_thaw_all():
    the_system.RUNS_ON._rsc_thaw()
    constants.CONST._rsc_thaw()
