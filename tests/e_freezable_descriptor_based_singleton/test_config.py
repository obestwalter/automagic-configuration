import pytest

from e_freezable_descriptor_based_singleton import the_system
from e_freezable_descriptor_based_singleton.app import some_important_function


@pytest.mark.usefixtures("rsc_thaw_all")
def test_monkeypatch_also_works_now(monkeypatch):
    # descriptors as opposed to properties make attr monkeypatching possible
    monkeypatch.setattr(the_system.RUNS_ON, "DEVBOX", True)
    monkeypatch.setattr(the_system.RUNS_ON, "CI", True)
    # this way the underlying mechanics can be circumvented - not great
    assert the_system.RUNS_ON.DEVBOX and the_system.RUNS_ON.CI


@pytest.mark.usefixtures("rsc_thaw_all")
@pytest.mark.parametrize(
    "text, fqdn, found_in_stdout",
    (
        ("Hi from some important function!", None, True),
        (
            "only want to do in the cloud",
            the_system._RUNS_ON._CRITERION_CLOUD_HOST,
            True,
        ),
        ("only want to do in the cloud", "random.machine", False),
        ("[devbox]", "random.machine", True),
        ("secondary", "random.machine", True),
        ("[ci]", the_system._RUNS_ON._CRITERION_CI_HOST, True),
        ("[cloud]", the_system._RUNS_ON._CRITERION_CI_HOST, False),
        ("[cloud]", "wherever", False),
    ),
)
def test_output_different_systems(monkeypatch, capsys, text, fqdn, found_in_stdout):
    if fqdn:
        # patching only necessary for basic constant now
        monkeypatch.setattr(the_system, "FQDN", f"extra.{fqdn}")
    some_important_function()
    assert (text in capsys.readouterr().out) == found_in_stdout
