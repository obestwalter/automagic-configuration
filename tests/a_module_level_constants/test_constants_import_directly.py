from pathlib import Path

from a_module_level_constants import the_system
from a_module_level_constants.app_import_directly import some_important_function


def test_const_on_ci(monkeypatch, capsys):
    monkeypatch.setattr(the_system, "FQDN", f"extra.{the_system._CRITERION_CI_HOST}")

    PLATFORM_NAME = the_system.determine_platform(the_system._CRITERION_CI_HOST)
    # just patch what is needed, but this is necessary everywhere it is used then
    DERIVED_PATH = _BASE_PATH = Path("/some/ci/path", "build-tmp", "some", "other")

    # I have to patch every value directly, even if one of them is derived
    from a_module_level_constants import app_import_directly

    monkeypatch.setattr(app_import_directly, "DERIVED_PATH", DERIVED_PATH)

    monkeypatch.setattr(app_import_directly, "PLATFORM_NAME", PLATFORM_NAME)
    monkeypatch.setattr(app_import_directly, "CLOUD", False)

    some_important_function()
    stdout = capsys.readouterr().out
    assert "[ci]" in stdout
    assert str(Path("/some/ci/path/build-tmp/some/other")) in stdout
