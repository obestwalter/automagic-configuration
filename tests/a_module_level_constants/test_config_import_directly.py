import pytest

from a_module_level_constants import the_system
from a_module_level_constants.app_import_directly import some_important_function


@pytest.mark.parametrize(
    "text, fqdn, found_in_stdout",
    (
        ("Hi from some important function!", None, True),
        (
            "only want to do in the cloud",
            the_system._CRITERION_CLOUD_HOST,
            True,
        ),
        ("only want to do in the cloud", "random.machine", False),
        ("[devbox]", "random.machine", True),
        ("secondary", "random.machine", True),
        ("[ci]", the_system._CRITERION_CI_HOST, True),
        ("[cloud]", the_system._CRITERION_CI_HOST, False),
        ("[cloud]", "wherever", False),
    ),
)
def test_output_different_systems(monkeypatch, capsys, text, fqdn, found_in_stdout):
    if fqdn:
        # one disadvantage of module level constants is that they can be imported in
        # different ways - directly or as part of a module import, that's why the only
        # safe way to patch them, is to patch them in all places where they are used
        # this gets very awkward very fast.
        # also see Ned Batchelders articles about mocks ("Why your mock ...")
        from a_module_level_constants import app_import_directly

        # this is starting to get horrible on so many levels so quickly ...
        from a_module_level_constants import the_system

        PLATFORM_NAME = the_system.determine_platform(fqdn)

        # I have to patch every value directly, even if one of them is derived
        monkeypatch.setattr(app_import_directly, "PLATFORM_NAME", PLATFORM_NAME)
        monkeypatch.setattr(
            app_import_directly, "CLOUD", PLATFORM_NAME == the_system._NAME_CLOUD
        )

    some_important_function()
    out = capsys.readouterr().out
    assert (text in out) == found_in_stdout
