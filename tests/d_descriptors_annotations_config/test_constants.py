from pathlib import Path

import pytest

# from d_descriptors_annotations_config.constants import CONST  # not affected by module reload
from d_descriptors_annotations_config import (
    constants,
    the_system,
)  # affected by module reload
from d_descriptors_annotations_config.app import initialize_app, some_important_function


def test_uninitialized_const_crashes_on_access(monkeypatch):
    with pytest.raises(AttributeError):
        print(constants.CONST.SOME_PATH)


def test_uninitialized_derived_str_const_crashes_on_access(monkeypatch):
    with pytest.raises(AttributeError):
        print(constants.CONST.DERIVED_INFO)


def test_uninitialized_derived_path_const_crashes_on_access(monkeypatch):
    with pytest.raises(AttributeError):
        print(constants.CONST.DERIVED_PATH)


def test_initialized_derived_const_works():
    initialize_app("/some/path", "sometext")
    assert isinstance(constants.CONST.SOME_OTHER_PATH, Path)
    assert isinstance(constants.CONST.DERIVED_INFO, str)
    assert str(constants.CONST.DERIVED_INFO) == "sometext-derived-value"
    # if there are tests after this one that expect an uninitialized CONST
    # this will be not going es expected again ... so it becomes tricky very fast
    # doesn't feel like this approach is worth the extra fuzz, at least not
    # without a very good reason.


def test_const_on_ci(monkeypatch, capsys):
    initialize_app("/some/ci/path/build-tmp", "sometext")
    monkeypatch.setattr(
        the_system, "FQDN", f"extra.{the_system.RUNS_ON._CRITERION_CI_HOST}"
    )
    some_important_function()
    stdout = capsys.readouterr().out
    assert "[ci]" in stdout
    assert str(Path("/some/ci/path/build-tmp/other/somefile")) in stdout
