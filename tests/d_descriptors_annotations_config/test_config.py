import pytest

from d_descriptors_annotations_config import constants, the_system
from d_descriptors_annotations_config.app import some_important_function, initialize_app


@pytest.fixture(scope="module", autouse=True)
def auto_initialize_app():
    initialize_app()
    yield
    # trying to reset status for other tests here ... not that easy though
    # "best" way? class object needs to be deleted
    #   => reload module that creates the class
    from importlib import reload

    assert hasattr(constants.CONST, "SOME_PATH")  # it's there
    reload(constants)
    assert not hasattr(constants.CONST, "SOME_PATH")  # it's not there
    # so it looks like this works here, but ...
    # pytest likely has imported all modules already when collecting the test functions.
    # Therefore all objects are loaded and attached to those module objects.
    # This makes it dependent then on **how** they were imported.
    # E.g. import CONST      => reload here has no effect
    # vs   import constants  => reload here has an effect as the same object is reloaded


@pytest.mark.parametrize(
    "text, system, expectation",
    (
        ("Hi from some important function!", None, True),
        (
            "only want to do in the cloud",
            the_system._RUNS_ON._CRITERION_CLOUD_HOST,
            True,
        ),
        ("only want to do in the cloud", "random.machine", False),
        ("[devbox]", "random.machine", True),
        ("secondary", "random.machine", True),
        ("[ci]", the_system._RUNS_ON._CRITERION_CI_HOST, True),
        ("[cloud]", the_system._RUNS_ON._CRITERION_CI_HOST, False),
        ("[cloud]", "wherever", False),
    ),
)
def test_output_different_systems(monkeypatch, capsys, text, system, expectation):
    if system:
        monkeypatch.setattr(the_system, "FQDN", f"extra.{system}")
    some_important_function()
    assert (text in capsys.readouterr().out) == expectation
